Problem Statement:
    Convert the existing CLI application to a fully featured webapplication using the data from SOcrata API to serve
    million users.


Requirements:
    1. Our users should be able to get Current open food trucks with paginated results and probably get direction
       to the food truck from their location.
    2. Provide users to search for any week of the day and any time .
    3. List food trucks based on distance between the locationid of the food truck and their current location

Scale:
    1. Since the requests are to list the food trucks, this is a ready heavy system.
    2. To avoid network latency and repetitive API calls to Socrata API, it is best if we have a data store for the food truck data.
    3. Traffic and Load Balancing the incoming requests with Load Balancers in the front of the app servers.


Data Model:

    At a high level, all the requests are currently made to the Socrata API using SqQL query language which results in
    a higher network latency to retrieve back the results which leads us to have a separate data store for storing and
    indexing the dataset for efficient access. For users to query the datastore, indexing should be read efficient, since
    while searching for nearby food trucks the users can see the results in realtime.

    Additionally, based on the nature of the data, it seems that we can add attributes to the dataset and hence we could
    use something like a NoSQL data store as it might be best fit in our case since we can horizontally scale on the requests and
    efficiently add or remove attributes to the existing data set without affecting the implementation. We might also need
    a block storage to store food truck menu, photos etc. For any future improvements like adding food truck reviews, we can use NoSQL too.

    We could also use something like DynamoDB for the NoSQL store and provision global secondary indexes for retrieving the
    results in single digit millisecond latency or something like Elasticsearch is a good option which are very powerful for searching capabilities.

High Level Design:

    We will need several application servers to serve all read requests with load balancers in front of them for traffic distributions.
    We could follow something similar to a microservice architectural pattern where we have the following services with their own functions.
    1. FoodTruck data generation service:
        A service to talk to SOCRATA API and store the food truck data set to the database which can be a service that is
        scheduled to run on a regular interval. Based on the nature of the data , we can probably schedule this weekly. We can also look up into
        options like Event Based or Pub/Sub model where when a change in dataset occurs, it triggers our service rather than pull based.
    2. FoodTruck Publish Service
        A service that pulls the data from the datastore and publish the results back  to the end user with paging capabilities.
        This service is responsible to serve the actual results either from a Cache or Data store.
    3. GetLocationService
        A service that determines the location from where the request is originated from to produce the best results for the
        end user and improver customer satisfaction

Caching :
    We could cache the results of the food trucks as they do not get updated very often which will improve the latency .We could definitely
    use something like Redis as the in-memory store and have proper eviction policy to ensure the data is not stale .

Deployment:
    1. All services can de deployed in cloud that autoscales based on the traffic .Probably a docker container .
    2. Ensure Disaster recovery is in place by replicating the data store and have services in pilot mode when there is
       any catastrophic emergencies to have an increased availability of the application.
    3. Ensure we have both monitoring and alerting capabilities on the entire system.
    4. Have detailed logging and improve the customer satisfaction with providing detailed error messages when needed.
    5. Have this whole system integrated with CI/CD

Last but not least, definitely a very attractive UI layer that provides easy navigation for the users to use the web application (E.g React )

Future Improvements:

    1. We could provide a recommendation service of similar food options that user's may be interested in.
    2. Provide option for a delivery service.
