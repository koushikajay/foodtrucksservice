from base import Base
from soql_query_parameter_builder import SoqlQueryParameterBuilder
from tabulate import tabulate
import argparse
from datetime import datetime
import logging


class FoodTruckService(Base):
    def __init__(self):
        Base.__init__(self)
        self.logger = logging.getLogger(__name__)

    def get_food_trucks(self, soql_clauses):
        pagination_required = True
        current_offset = self.data_offset
        while pagination_required:
            soql_clauses['offset'] = current_offset
            soql_query_parameter_builder = SoqlQueryParameterBuilder(soql_clauses)
            list_of_query_parameters = soql_query_parameter_builder.generate_url_path_with_query_parameters()
            querystring_parameters = "&".join(list_of_query_parameters)
            self.logger.debug(f'Query String Parameters: {querystring_parameters}')
            queryUrl = f'{self.api_endpoint}?{querystring_parameters}'
            # self.logger.info(f'\"{queryUrl}\"')
            response = self.get(queryUrl)

            if self.food_trucks_not_available(response):
                if current_offset == 0:
                    print("No Open Food Trucks at this time .Sorry, Please check back later. ")
                    exit(0)
                print("You have viewed all the Food trucks for the current open window. Would you like to see "
                      "previous page results?")
                options = {
                    '1': 'YES',
                    '2': 'NO',
                }

                for option, option_display_text in options.items():
                    print(f'{option}: {option_display_text}')
                input_value = input("Please choose one from the above specified options:  ")
                if input_value == '1':
                    current_offset = self.reset_prev_offset(current_offset)
                    continue
                elif input_value == '2':
                    print('Thank you for viewing the SF Food Trucks. GoodBye!!')
                    break
                else:
                    print("Invalid Choice!!")
                    break
            self.page_results(response, soql_clauses['select'])

            options = {
                '1': f'NEXT {soql_clauses["limit"]} RESULTS',
                '2': 'PREV OR CURRENT PAGE RESULTS',
                '3': 'EXIT'
            }

            for option, option_display_text in options.items():
                print(f'{option}: {option_display_text}')
            input_value = input("Please choose one from the above specified options:  ")
            if input_value in options:
                if input_value == '1':
                    current_offset = self.get_next_offset(current_offset)
                elif input_value == '2':
                    current_offset = self.reset_prev_offset(current_offset)
                elif input_value == '3':
                    print('Thank you for viewing the SF Food Trucks. GoodBye!!')
                    break
                else:
                    break
            else:
                print(f'Invalid Choice')
                print("Resetting to initial results")
                current_offset = self.data_offset

    def page_results(self, response, fields):
        self.logger.info(f'Displaying {self.display_limit} Food Truck Results ..')
        tabulated_food_trucks = []
        for food_truck in response:
            tabulated_food_trucks.append([(food_truck[k]) for k in fields])
        print(tabulate(tabulated_food_trucks, tablefmt='fancy_grid', headers=fields))

    def get_next_offset(self, current_offset):
        self.logger.debug(f'Obtaining the next offset to pull rest of the data')
        current_offset += self.display_limit
        self.logger.debug(f'Next Offset : {current_offset}')
        return current_offset

    def reset_prev_offset(self, current_offset):
        self.logger.debug(f'Resetting the offset to a previous value.')
        current_offset -= self.display_limit
        if current_offset < 0:
            current_offset = 0
        self.logger.info(f'Previous Offset : {current_offset}')
        return current_offset

    def is_pagination_required(self, current_offset):
        self.logger.debug(f'Checking to see if there are more results ..  ')
        return current_offset >= 10

    def food_trucks_not_available(self, response):
        self.logger.debug(f'Checking if there are any more food trucks available...')
        return len(response) == 0

    def dayorder_validator(self, value):
        self.logger.debug(f'Validating the day order (day of the week)')
        if 0 <= int(value) <= 6:
            dayorder = str(value)
            self.logger.debug(f'Requested Day to find open food trucks: {dayorder}')
            return dayorder
        else:
            raise argparse.ArgumentTypeError(f'{value} must be an integer between 0 and 6 (inclusive)')

    def servicetime_validator(self, value):
        self.logger.debug("Validating the provided input service time is in the correct 24hr format (%H:%M)")
        try:
            service_time = datetime.strptime(value, '%H:%M')
            self.logger.debug(f'Requested service time {service_time} is a valid 24hr format time. ')
            return service_time.strftime('%H:%M')
        except Exception:
            raise argparse.ArgumentTypeError(f'Please modify the servicetime parameter value {value}. It must follow '
                                             f'24hr format time (Between 00:00 , 23:59 ) must follow HH:MM format')
