from dateutils import DateUtils
from base import Base
from soql_clause import SoqlClause
from food_truck_service import FoodTruckService
import argparse
import logging


class ShowOpenFoodTrucks:
    def __init__(self):
        self.date_utils = DateUtils()
        self.food_truck_service = FoodTruckService()
        self.base = Base()

    def get_open_food_trucks(self, servicetime=None, **kwargs):
        food_truck = kwargs
        print(
            f'Current Date: {self.date_utils.current_date.strftime("%Y-%m-%d")} , TimeZone: {self.date_utils.timezone}')
        food_truck_soql_query_clauses = SoqlClause(select=['applicant', 'location'],
                                                   where=[f'start24<"{servicetime}"',
                                                          f'end24>"{servicetime}"',
                                                          f'dayorder={food_truck["dayorder"]}'],
                                                   order_by=["applicant"],
                                                   limit=self.base.display_limit,
                                                   offset=self.base.data_offset)
        self.food_truck_service.get_food_trucks(food_truck_soql_query_clauses.__repr__())


if __name__ == '__main__':
    ft = ShowOpenFoodTrucks()
    parser = argparse.ArgumentParser(add_help=True)

    parser.add_argument('--dayorder', type=FoodTruckService().dayorder_validator,
                        default=DateUtils().get_modified_isoweekday(),
                        dest='dayorder',
                        help="Specify the week of the day (0-Sunday,1-Monday ..)")
    parser.add_argument('--servicetime', type=FoodTruckService().servicetime_validator,
                        default=DateUtils().get_24_hour_format_of_current_time(), dest='servicetime',
                        help="Specify the time you are looking for in 24Hr format(HH:MM: E.g 01:00) ")
    parser.add_argument('-v', "--verbose", dest="verbosity", action="count", default=0,
                        help="Verbosity (between 1-4 occurrences with more leading to more "
                             "verbose logging). CRITICAL=0, ERROR=1, WARN=2, INFO=3, "
                             "DEBUG=4")
    log_levels = {
        0: logging.CRITICAL,
        1: logging.ERROR,
        2: logging.WARN,
        3: logging.INFO,
        4: logging.DEBUG,
    }
    args = parser.parse_args()
    logging.basicConfig(level=log_levels[args.verbosity], format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    logger = logging.getLogger(__name__)
    ft.get_open_food_trucks(**vars(args))
