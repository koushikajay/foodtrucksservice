import datetime
import logging
import pytz


class DateUtils:
    def __init__(self):
        self._24_HOUR_FORMAT = '%H:%M'
        self.timezone = "US/Pacific"
        self.current_date = datetime.datetime.now(pytz.timezone(self.timezone))
        self.logger = logging.getLogger(__name__)
        self.logger.debug(f'Today\'s Date : {self.current_date}, TimeZone: {self.timezone}')

    def get_modified_isoweekday(self):
        self.logger.debug(f'Obtaining the current day of the week... ')
        isoweekday = self.current_date.isoweekday() % 7
        self.logger.info(f'Current Day of the week :{isoweekday}')
        return isoweekday

    def get_24_hour_format_of_current_time(self):
        self.logger.debug(f'Obtaining the 24 hr format of current time ...')
        _24_hour_format = self.current_date.strftime(self._24_HOUR_FORMAT)
        self.logger.info(f'Current Time in 24hr Format: {_24_hour_format}')
        return _24_hour_format
