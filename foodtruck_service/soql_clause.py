from food_truck import FoodTruck
import logging


class SoqlClause(object):
    def __init__(self, **kwargs):
        self.select = kwargs.pop('select', None)
        if self.select == ['*']:
            ft = FoodTruck()
            self.select = ft.__dict__.keys()
        self.where = kwargs.pop('where', None)
        self.group_by = kwargs.pop('group_by', None)
        self.order_by = kwargs.pop('order_by', None)
        self.limit = kwargs.pop('limit', None)
        self.offset = kwargs.pop('offset', None)
        self.query = kwargs.pop('query', None)
        self.logger = logging.getLogger(__name__)

    def __repr__(self):
        return self.__dict__
