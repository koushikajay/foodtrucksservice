import logging


class FoodTruck:
    def __init__(self, **kwargs):
        self.applicant = kwargs.pop('applicant', None)
        self.location = kwargs.pop('location', None)
        self.logger = logging.getLogger(__name__)

    def __repr__(self):
        self.logger.debug(f'Food Truck Name:{self.applicant} , Location: {self.location}')
        return self.__dict__
