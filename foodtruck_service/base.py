import requests
import logging

class Base:
    headers = {'Content-Type': 'application/json',
               'Accept': 'application/json'}

    def __init__(self):
        self._api_base_url = "http://data.sfgov.org/resource"
        self._resource_identifier = "jjew-r69b.json"#"bbb8-hzi6.json"
        self._api_token = ""
        self._display_limit = 10
        self._data_offset = 0
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def _request(method, url):
        try:
            response = requests.request(method, url)
            response.raise_for_status()

        except requests.exceptions.HTTPError as errh:
            raise errh
        except requests.exceptions.ConnectionError as errc:
            raise errc
        except requests.exceptions.Timeout as errt:
            raise errt
        except requests.exceptions.RequestException as err:
            raise err
        return response.json()

    def get(self, url):
        self.logger.debug(f'Target URL to obtain the data:{url}')
        return self._request('GET', url)

    @property
    def display_limit(self):
        return self._display_limit

    @property
    def api_endpoint(self):
        api_endpoint = f'{self._api_base_url}/{self._resource_identifier}'
        return api_endpoint

    @property
    def data_offset(self):
        return self._data_offset

    @data_offset.setter
    def data_offset(self, offset_value):
        self.logger.debug(f'Setting the current offset value to {offset_value}')
        self._data_offset = offset_value
