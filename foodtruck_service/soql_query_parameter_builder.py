import logging


class SoqlQueryParameterBuilder:
    def __init__(self, soql_query_params):
        self.soql_query_params = soql_query_params
        self.logger = logging.getLogger(__name__)

    def build_select_statement(self):
        select = self.soql_query_params.get('select', None)
        return f'$select={",".join(select)}'

    def build_where_statement(self, operator='AND'):
        where = self.soql_query_params.get('where', None)
        where_expression = f' {operator} '.join(where)
        return f'$where={where_expression}'

    def build_group_by_statement(self):
        group_by = self.soql_query_params.get('group_by', None)
        return f'$group={",".join(group_by)}'

    def build_order_by_statement(self, order='ASC'):
        order_by = self.soql_query_params.get('order_by', None)
        return f'$order={",".join(order_by)} {order}'

    def build_limit_statement(self):
        limit = self.soql_query_params.get('limit', None)
        return f'$limit={limit}'

    def build_offset_statement(self):
        offset = self.soql_query_params.get('offset', None)
        return f'$offset={offset}'

    def build_query_statement(self):
        query = self.soql_query_params.get('query', None)
        return f'$query={query}'

    def generate_url_path_with_query_parameters(self):
        self.logger.debug("Generating the URL path with Query Parameters..")
        all_query_parameters = []
        for query_parameter_name, query_parameter_value in self.soql_query_params.items():
            if query_parameter_value is not None:
                build = f'build_{query_parameter_name}_statement'
                if hasattr(self, build) and callable(getattr(self, build)):
                    func = getattr(self, build)
                    all_query_parameters.append(func())
        return all_query_parameters
