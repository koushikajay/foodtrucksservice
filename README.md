<h1>SF FOOD TRUCK SERVICE</h1>

A simple command-line program that will print out the list of food trucks .

Data Source: https://dev.socrata.com/foundry/data.sfgov.org/jjew-r69b

<h3>Implementation details</h3>

<h4><i>Problem Statement</i></h4>
The program that prints out a list of food trucks that are open at the current date and current time,
when the program is being run. So if I run the program at noon on a Friday,
 I should see a list of all the food trucks that are open then. 

<h4><i>Implementation</i></h4> 
A main program `show_open_food_trucks.py` initiates the call to other service classes to list out all the food trucks.
By default this program will tabulate 10 trucks sorted alphabetically, that are open at the time the program is being executed
and provide options for the end user to navigate the cli tool for additional results if there are more than 10 trucks in the current window.
However, the total number of trucks to be displayed is configurable and can be changed in the `base.py` class of the module. 
The implementation uses the `SOCRATA API` query functionality through a query language `SoQL` which filters the open food trucks 
based on `dayorder` .`start24` and `end24` attributes of the data.

Additionally, this implementation also provides two option for the end user to find out all the open trucks based on: 

1. A specific day of the week (`--dayorder`) 
2. A specific time to check all open food trucks (`--servicetime`)

<h4><i>Setup and Run</i></h4> 

```NOTE:``` All the below steps are based on Centos7 Linux distribution
1. Install Python3 
    - Install Python3
        - `yum install -y python3`
    - Verify Python installation
        -  Type in `python3` to see the version of Python 3 installed on your system
2. Setup Virtual Environment
    - `python3 -m venv 'venv_name' `
    - source `venv_name`/bin/activate
3. Install Dependencies
    - Copy the source files and unzip them to a directory
    - Navigate to the food truck service module
        - `cd foodtruck_service`
        - `pip3 install -r requirements.txt`
3. Execute the program
    - `python3 show_open_food_trucks.py` - Displays Open Food Trucks as of execution time of the program
    
 
 <h4><i>Additional Options</i></h4>

For Help \
`python3 show_open_food_trucks.py --help`

Food Trucks for specific day of the week \
`python3 show_open_food_trucks.py --dayorder 0` where dayorder is value between 0 and 6

Food trucks for specific time of the day \
`python3 show_open_food_trucks.py --servicetime 10:00` where servicetime is a 24hr format time

For Verbose logs \
`python3 show_open_food_trucks.py --verbose` provide detailed DEBUG logs

Both `dayorder` and `servicetime` are mutually inclusive parameters to the script.

<h3>Future Improvements</h3>

1. We could provide end users with additional parameters to find food trucks based on `locationid` of the food truck.
2. Provide an option to display the results in different formats.
3. Expand the CLI tool into a complete web application .
4. Subscription/Notification to end users when new Food trucks are added.
5. Provide configurable paging option and allow users to filter on type of food.
